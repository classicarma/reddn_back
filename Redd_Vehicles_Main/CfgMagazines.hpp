	
	
	class CfgAmmo
	{
		
		class B_30mm_HE_Tracer_Red;
		class B_30mm_APFSDS_Tracer_Red;
		class SmokeLauncherAmmo;
		class FlareCore;
		class M_Titan_AT;
		class Sh_82mm_AMOS;
		class Flare_82mm_AMOS_White;
		class Smoke_82mm_AMOS_White;
		class APERSBoundingMine_Range_Ammo;
		
		class Redd_B_30mm_HE_Tracer_Red: B_30mm_HE_Tracer_Red
		{
			
			typicalSpeed = 1050;
			irLock = 0;
			airLock = 0;
			laserLock = 0;
			nvlock = 0;
			canLock = 0;
			tracerScale = 2;
			tracerStartTime = 0.07;
			tracerEndTime = 5.7;
			hit = 25;
			indirectHit = 15;
			indirectHitRange = 5;
			caliber = 2.5;
			explosive = 0.95;

			class CamShakeFire
			{
				
				power = 1;
				duration = 1;
				frequency = 20;
				distance = 10;
				
			};
			
		};
		
		class Redd_B_30mm_APFSDS_Tracer_Red: B_30mm_APFSDS_Tracer_Red
		{
			
			typicalSpeed = 1050;
			irLock = 0;
			airLock = 0;
			laserLock = 0;
			nvlock = 0;
			canLock = 0;
			tracerScale = 2;
			tracerStartTime = 0.07;
			tracerEndTime = 5.7;
			hit = 100;
			caliber = 5.2;

			class CamShakeFire
			{
				
				
				power = 1;
				duration = 1;
				frequency = 20;
				distance = 10;
				
			};
			
		};
		
		class Redd_B_35mm_AA_Tracer_Red: B_30mm_HE_Tracer_Red
		{
			
			typicalSpeed = 1400;
			airFriction=-0.00015;
			tracerEndTime = 100;
			tracerStartTime = 0.07;
			hit = 80;
			indirectHit = 6;
			indirectHitRange = 4;
			visibleFireTime = 5;
			explosive = 1;
			cost = 42;
			caliber = 4;
			tracerScale = 2.2;

			class CamShakeFire
			{
				
				power = 1;
				duration = 1;
				frequency = 20;
				distance = 10;
				
			};

		};

		class Redd_B_40mm_APFSDS_Tracer_Red: B_30mm_APFSDS_Tracer_Red
		{
			
			typicalSpeed = 1400;
			airFriction=-0.00015;
			tracerEndTime = 100;
			tracerStartTime = 0.07;
			hit = 150;
			indirectHit = 8;
			indirectHitRange = 0.2;
			visibleFireTime = 5;
			cost = 100;
			caliber = 8;
			tracerScale = 2.2;

			class CamShakeFire
			{
				
				power = 1;
				duration = 1;
				frequency = 20;
				distance = 10;
				
			};
			
		};
		
		class Redd_SmokeLauncherAmmo: SmokeLauncherAmmo 
		{
			
			muzzleEffect = "";
			effectsSmoke = "EmptyEffect";
			weaponLockSystem = "1 + 2 + 4";
			hit = 1;
			indirectHit = 0;
			indirectHitRange = 0;
			timeToLive = 10.0;
			thrustTime = 10.0;
			airFriction = -0.1;
			simulation = "shotCM";
			model = "\A3\weapons_f\empty";
			maxControlRange = 50;
			initTime = 2;
			aiAmmoUsageFlags = "4 + 8";
			
		};
		
		class Redd_SmokeShellSubVehicle: FlareCore 
		{
		
			model = "\A3\weapons_f\ammo\flare_white";
			deflecting = 10;
			lightColor[] = {1, 0.500000, 0, 0.200000};
			useFlare = 1;
			flareSize = 1;
			smokeColor[] = {1, 1, 1, 0.500000};
			muzzleEffect = "BIS_fnc_effectFiredRifle";
			effectFlare = "CounterMeasureFlare";
			brightness = 0.500000;
			size = 0.200000;
			triggerTime = 0;
			triggerSpeedCoef = 1;
			
		};
		
		class Redd_Milan_AT: M_Titan_AT
		{
			
			canLock = 0;
			irLock = 0;
			airLock = 0;
			laserLock = 0;
			nvlock = 0;
			manualControl = 1;
			maxControlRange = 3000;
			maxSpeed = 270;
			thrustTime = 2;
			thrust=45;
			fuseDistance = 65;
			
		};

		class Redd_TOW_AT: Redd_Milan_AT
		{

			maxControlRange = 4000;

		};

		class Redd_Sh_120mm_AMOS: Sh_82mm_AMOS
		{

			hit=180;
			indirectHit=60;
			indirectHitRange=25;
			explosive=1;

		};

		class Redd_Sh_120mm_AMOS_annz: Redd_Sh_120mm_AMOS{};

		class Redd_Flare_120mm_AMOS_White: Sh_82mm_AMOS
		{

			hit=0;
			indirectHit=0;
			indirectHitRange=0;
			explosive=0;
			CraterEffects = "";
			ExplosionEffects = "";
			SoundSetExplosion[] = {};
			soundHit1[] = {};
			soundHit2[] = {};
			soundHit3[] = {};
			soundHit4[] = {};
			multiSoundHit[] = {};
			
		};

		class Redd_Smoke_120mm_AMOS_White: Smoke_82mm_AMOS_White{};

		class Redd_APERSBoundingMine_Range_Ammo: APERSBoundingMine_Range_Ammo
		{

			hit = 75;
			indirectHit = 60;
			indirectHitRange = 35;
			explosionEffects = "Redd_BoundingMineExplosion";
			CraterEffects = "Redd_BoundingMineCrater";

		};

		
	};
	
	class CfgMagazines 
	{
		
		class 140Rnd_30mm_MP_shells_Tracer_Red;
		class 60Rnd_30mm_APFSDS_shells_Tracer_Red;
		class 1000Rnd_762x51_Belt_Red;
		class 2Rnd_GAT_missiles;
		class SmokeLauncherMag;
		class VehicleMagazine;
		class 8Rnd_82mm_Mo_shells;
		class 8Rnd_82mm_Mo_Flare_white;
		class 8Rnd_82mm_Mo_Smoke_white;


		class Redd_MK20_HE_Mag: 140Rnd_30mm_MP_shells_Tracer_Red
		{
			
			scope = 2;
			displayName = "20mm HEI";
			displayNameShort = "HEI";
			ammo = "Redd_B_30mm_HE_Tracer_Red";
			count = 700;
			tracersEvery = 1;
			muzzleImpulseFactor[]={0.05,0.05};
			
		};

		class Redd_MK20_HE_Mag200: Redd_MK20_HE_Mag 
		{

			count = 200;
			muzzleImpulseFactor[]={0,0};

		};
		
		class Redd_MK20_AP_Mag: 60Rnd_30mm_APFSDS_shells_Tracer_Red
		{
			
			scope = 2;
			displayName = "20mm APDS";
			displayNameShort = "APDS";
			ammo = "Redd_B_30mm_APFSDS_Tracer_Red";
			count = 350;
			tracersEvery = 1;
			muzzleImpulseFactor[]={0.05,0.05};
			
		};

		class Redd_MK20_AP_Mag120: Redd_MK20_AP_Mag 
		{

			count = 120;
			muzzleImpulseFactor[]={0,0};

		};

		class Redd_35mm_HE_Mag: VehicleMagazine
		{

			scope = 2;
			displayName = "35mm FAPDS";
			displayNameShort = "FAPDS";
			ammo = "Redd_B_35mm_AA_Tracer_Red";
			count = 640;
			tracersEvery = 1;
			initSpeed = 1400;
			muzzleImpulseFactor[]={0.05,0.05};

			maxLeadSpeed = 500;
			nameSound = "cannon";

		};

		class Redd_35mm_AP_Mag: VehicleMagazine
		{

			scope = 2;
			displayName = "35mm HVAPDS-T";
			displayNameShort = "HVAPDS-T";
			ammo = "Redd_B_40mm_APFSDS_Tracer_Red";
			count = 40;
			tracersEvery = 1;
			initSpeed = 1400;
			muzzleImpulseFactor[]={0.05,0.05};

			maxLeadSpeed = 500;
			nameSound = "cannon";

		};

		class Redd_Mg3_Mag: 1000Rnd_762x51_Belt_Red
		{
			
			scope = 2;
			displayName = "$STR_Redd_MG3_500-Schuss-Gurtkasten";
			displayNameShort = "";
			ammo = "B_762x51_Tracer_Red";
			count = 500;
			tracersEvery = 3;
			
		};

		class Redd_Mg3_Mag_120: Redd_Mg3_Mag
		{
			
			displayName = "$STR_Redd_MG3_120-Schuss-Gurtkasten";
			count = 120;
			
		};
		
		class Redd_Milan_Mag: 2Rnd_GAT_missiles
		{
			
			scope = 2;
			displayName = "Milan";
			displayNameShort = "";
			ammo = "Redd_Milan_AT";
			count = 1;
			initSpeed = 180;
			maxLeadSpeed = 60;
			lockType = 1;
			
		};

		class Redd_TOW_Mag: Redd_Milan_Mag
		{
			
			displayName = "TOW";
			ammo = "Redd_TOW_AT";
			
		};
		
		class Redd_SmokeLauncherMag: SmokeLauncherMag 
		{
			
			ammo = "Redd_SmokeLauncherAmmo";
		
		};

		class Redd_8Rnd_120mm_Mo_shells: 8Rnd_82mm_Mo_shells
		{

			displayName="$STR_Redd_120mm_HE";
			displayNameShort="$STR_Redd_120mm_HE";
			ammo="Redd_Sh_120mm_AMOS";


		};

		class Redd_8Rnd_120mm_Mo_annz_shells: 8Rnd_82mm_Mo_shells
		{

			displayName="$STR_Redd_120mm_HE_annz";
			displayNameShort="$STR_Redd_120mm_HE_annz";
			ammo="Redd_Sh_120mm_AMOS_annz";

		};

		class Redd_8Rnd_120mm_Mo_Flare_white: 8Rnd_82mm_Mo_shells
		{

			
			displayName="$STR_Redd_120mm_Flare";
			displayNameShort="$STR_Redd_120mm_Flare";
			ammo="Redd_Flare_120mm_AMOS_White";

		};

		class Redd_8Rnd_120mm_Mo_Smoke_white: 8Rnd_82mm_Mo_Smoke_white
		{

			displayName="$STR_Redd_120mm_Smoke";
			displayNameShort="$STR_Redd_120mm_Smoke";
			ammo="Redd_Smoke_120mm_AMOS_White";

		};

		class Redd_1Rnd_120mm_Mo_shells: Redd_8Rnd_120mm_Mo_shells
		{

			count = 1;
			model = "Redd_Vehicles_Main\data\120mm_Moerserpatrone_he.p3d";
        	picture = "\Redd_Vehicles_Main\data\m120_he_pre_ca.paa";
        	mass = 50;

		};

		class Redd_1Rnd_120mm_Mo_annz_shells: Redd_8Rnd_120mm_Mo_annz_shells
		{

			count = 1;
			model = "Redd_Vehicles_Main\data\120mm_Moerserpatrone_annz.p3d";
        	picture = "\Redd_Vehicles_Main\data\m120_he_pre_ca.paa";
        	mass = 50;

		};

		class Redd_1Rnd_120mm_Mo_Flare_white: Redd_8Rnd_120mm_Mo_Flare_white
		{

			count = 1;
			model = "Redd_Vehicles_Main\data\120mm_Moerserpatrone_illum.p3d";
        	picture = "\Redd_Vehicles_Main\data\m120_illum_pre_ca.paa";
        	mass = 50;

		};

		class Redd_1Rnd_120mm_Mo_Smoke_white: Redd_8Rnd_120mm_Mo_Smoke_white
		{

			count = 1;
			model = "Redd_Vehicles_Main\data\120mm_Moerserpatrone_nebel.p3d";
        	picture = "\Redd_Vehicles_Main\data\m120_nebel_pre_ca.paa";
        	mass = 50;

		};
	
	};