

	//Triggerd by BI eventhandler "fired" when milan was fired

	_vehicle = _this select 0;

	_rocket = nearestObject [_vehicle,"MissileBase"]; //find fired missile to get its direction

	//Calc direction for empty tube
	_dir = getDir _rocket;
	_dirV = vectorDir _rocket;
	_upV = vectorUp _rocket;
	_speed = -20;

	_velocity = [sin _dir * _speed, cos _dir * _speed, 0];//Calc velocity for empty tube

	_tube = "Redd_Milan_Rohr" createVehiclelocal [0,0,0];
	_tube setVectorDirAndUp [_dirV,_upV]; //Sets dir and up for empty tube
	_tube attachTo [_vehicle, [0,0,0], "konec rakety"]; //Sets empty tube on its correct position on model
	detach _tube;
	_tube setVectorDirAndUp [_dirV,_upV]; //Sets dir and up for empty tube, second time
	_tube setVelocity _velocity; 
	_tube allowDamage false;
	[_tube] spawn redd_fnc_Delete_Tube; //spawns function to delete empty tube on ground after 2 minutes

	