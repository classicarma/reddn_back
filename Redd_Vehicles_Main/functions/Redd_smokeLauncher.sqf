// by commy2, based on BI's smoke script, edit Redd

_this spawn {
    _vehicle = _this select 0;
    _count = getNumber (configFile >> "CfgVehicles" >> typeOf _vehicle >> "smokeLauncherGrenadeCount");
	_vel = GetNumber (configFile >> "CfgVehicles" >> typeof _vehicle >> "smokeLauncherVelocity");
	_angle = GetNumber (configFile >> "CfgVehicles" >> typeof _vehicle >> "smokeLauncherAngle");
    _onTurret = GetNumber (configFile >> "CfgVehicles" >> typeof _vehicle >> "smokeLauncherOnTurret");
    _dir = 0;
    _i = _count;

    while {_i < _count + _count} do 
	{
        if (_onTurret == 1) then 
        {
		    _dir = _vehicle weaponDirection ((weapons _vehicle) select 0);
		    _dir = (((_dir select 0) atan2 (_dir select 1))+200)%360;

        }
        else
        {

            _dir = vectorDir _vehicle;
            _dir = (((_dir select 0) atan2 (_dir select 1))+200)%360;

        };
		
        _i = _i + 1;
        _smokePos = _vehicle selectionPosition format ["smoke%1_Pos", _i];
        _smokeDir = _vehicle selectionPosition format ["smoke%1_Dir", _i];
		
        _smokeSpawn = 
		[
		
            (_smokePos select 0) + 4 * ((_smokeDir select 0) - (_smokePos select 0)),
            (_smokePos select 1) + 4 * ((_smokeDir select 1) - (_smokePos select 1)),
            (_smokePos select 2) + 4 * ((_smokeDir select 2) - (_smokePos select 2))
			
        ];
		
        _smokePos = _vehicle modelToWorld _smokePos;
        _smokeDir = _vehicle modelToWorld _smokeDir;
        _shellDir = ((_smokeDir select 0) - (_smokePos select 0)) atan2 ((_smokeDir select 1) - (_smokePos select 1));
		
		_Vdir = 50;	                     //angle of elevation. Temporary: launch all grenades at same angle
		//derive velocity
		_vH = _vel * cos _Vdir;          //horizontal component of velocity
		_vV = _vel * sin _Vdir;
		_vV = _vV *(1.2);
		
		_deltaDir = _angle/_count;			//degrees between grenades.
		_arc = _deltaDir*(_count-1);
		
		_Hdir = ((_i*_deltaDir)+_dir) - _arc/2; //relative direction around vehicle
		_Gvel = [_vH *sin(_Hdir), _vH*cos (_Hdir), _vV]; //initial grenade velocity
		_shellVelocity = [_vH *sin(_Hdir), _vH*cos (_Hdir), _vV]; //initial grenade velocity
		
		_initDist = (((boundingBox _vehicle) select 1) select 2)-(((boundingBox _vehicle) select 0) select 2);
		_posV = visiblePositionASL _vehicle;
		
		//find starting position for grenades
		_pH = _initDist * cos _Vdir;     //initial distance horizontally away from vehicle center to create grenade
		_pV = _initDist * sin _Vdir;     //vertical distance

		//create / launch the grenade
		_smokeShell = "SmokeShellVehicle" createVehicleLocal [0,0,0];
		_smokeShell setPosASL [(_pH * sin _Hdir) + (_posV select 0), (_pH * cos _Hdir)+ (_posV select 1), _pV+ (_posV select 2)];
		_smokeShell setVectorUp (_Gvel call BIS_fnc_unitVector);
		_smokeShell setVelocity _Gvel;
		_smokeShell say3D "Redd_SmokeLauncher_Fire";
		
        drop [
            ["\A3\data_f\ParticleEffects\Universal\Universal", 16, 12, 1, 16],
            "",
            "Billboard",
            1,
            1.1,
            _smokeDir,
            [0, 0, 0.5],
            0,
            1.275,
            1,
            0.025,
            [0.72, 1.68, 2.4, 2.88, 3.36, 3.84, 4.32],
            [[0.9, 0.9, 0.9, 0.144], [0.9, 0.9, 0.9, 0.0648], [0.9, 0.9, 0.9, 0.0216], [0.9, 0.9, 0.9, 0.0018]],
            [0.2],
            1,
            0.04,
            "",
            "",
            ""
        ];
        _smokeShell spawn {
            sleep 1.2;

            _soundSource = "#lightpoint" createVehicleLocal getPos _this;
            _soundSource setLightBrightness 0;
			_soundSource say3D "Redd_SmokeLauncher_Explosion";

            _light = "#lightpoint" createVehicleLocal getPos _this;
            _light setLightBrightness 0.5;
            _light setLightAmbient [0.5, 0.5, 0.5];
            _light setLightColor [0.5, 0.5, 0.5];
            _light setLightDayLight true;

            drop [
                ["\A3\data_f\ParticleEffects\Universal\Universal", 16, 2, 32],
                "",
                "Billboard",
                0.1,
                0.12,
                getPos _this,
                [0.5, 0.5, 0.5],
                0,
                1.277,
                8,
                0.075,
                [0.1, 3],
                [[1, 1, 1, -2], [1, 1, 1, -2], [1, 1, 1, -1], [1, 1, 1, -0]],
                [3 + random 2],
                1,
                0,
                "",
                "",
                ""
            ];

            for "_i" from 1 to 4 do {
                _shellVelocity = velocity _this;

                switch (_i) do {
                    case 1 : {_shellVelocity = [-0.5 * (_shellVelocity select 1), 0.5 * (_shellVelocity select 0), 5]};
                    case 2 : {_shellVelocity = [-0.5 * (_shellVelocity select 1), 0.5 * (_shellVelocity select 0), -5]};
                    case 3 : {_shellVelocity = [0.5 * (_shellVelocity select 1), -0.5 * (_shellVelocity select 0), 5]};
                    case 4 : {_shellVelocity = [0.5 * (_shellVelocity select 1), -0.5 * (_shellVelocity select 0), -5]};
                };

                _smokeShell = "Redd_SmokeShellSubVehicle" createVehicleLocal [0, 0, 0];
                _smokeShell setPos (getPos _this);
                _smokeShell setVectorDir (_shellVelocity call BIS_fnc_unitVector);
                _smokeShell setVelocity _shellVelocity;

                _smokeShell spawn {
                    sleep 0.2;

                    _smoke1 = "#particlesource" createVehicleLocal getpos _this;
                    _smoke1 setParticleParams [
                        ["\A3\data_f\ParticleEffects\Universal\Universal", 16, 7, 48, 1],
                        "",
                        "Billboard",
                        1,
                        10,
                        [0, 0, 0],
                        [0, 0, 0.5],
                        0,
                        1.277,
                        1,
                        0.025,
                        [0.5, 8, 12, 15],
                        //[[1, 1, 1, 0.7], [1, 1, 1, 0.5], [1, 1, 1, 0.25], [1, 1, 1, 0]],
						[[0.9, 0.9, 0.9, 0.144], [0.9, 0.9, 0.9, 0.0648], [0.9, 0.9, 0.9, 0.0216], [0.9, 0.9, 0.9, 0.0018]],//Color
                        [0.2],
                        1,
                        0.04,
                        "",
                        "",
                        _this
                    ];
                    _smoke1 setParticleRandom [2, [0.3, 0.3, 0.3], [1.5, 1.5, 1], 20, 0.2, [0, 0, 0, 0.1], 0, 0, 360];
                    _smoke1 setDropInterval 0.2;

                    _smoke2 = "#particlesource" createVehicleLocal getpos _this;
                    _smoke2 setParticleParams [
                        ["\A3\data_f\ParticleEffects\Universal\Universal", 16, 12, 7, 0],
                        "",
                        "Billboard",
                        1,
                        5,
                        [0, 0, 0],
                        [0, 0, 0.5],
                        0,
                        1.277,
                        1,
                        0.025,
                        [0.5, 8, 12, 15],
                        [[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 0.5], [1, 1, 1, 0]],
                        [0.2],
                        1,
                        0.04,
                        "",
                        "",
                        _this
                    ];
                    _smoke2 setParticleRandom [2, [0.3, 0.3, 0.3], [1.5, 1.5, 1], 20, 0.2, [0, 0, 0, 0.1], 0, 0, 360];
                    _smoke2 setDropInterval 0.15;

                    sleep (55 + random 3);
                    deleteVehicle _this;
                };
            };
            deleteVehicle _this;
            sleep 0.12;
            deleteVehicle _light;
            sleep 2;
            deleteVehicle _soundSource;
        };
        sleep (0.05 + random 0.05);
    };
};
nil
