	
	
	class CfgPatches
	{
		
		class Redd_Vehicles_Main
		{
			
			units[] = 
			{

				"Redd_Milan_Rohr",
				"Redd_Wiesel_TOW_Wreck",
				"Redd_Wiesel_MK20_Wreck",
				"Redd_Fuchs_Wreck",
				"Redd_Marder_Wreck",
				"Redd_Gepard_Wreck",
				
			};
			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"A3_Weapons_F","A3_Sounds_f"};
			
		};
		
	};