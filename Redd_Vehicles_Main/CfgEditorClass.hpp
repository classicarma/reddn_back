	
	
	class CfgEditorCategories
	{
		
		class Redd_Vehicles
		{
			
			displayName = "Redd'n'Tank Vehicles";
			
		};
		
	};
	
	class CfgEditorSubcategories
	{

		class Redd_Spz
		{
					
			displayName = "$STR_Redd_Schuetzenpanzer";
				
		};

		class Redd_Static
		{
					
			displayName = "$STR_Redd_Statisch";
				
		};

		class Redd_Bags
		{
					
			displayName = "$STR_Redd_Rucksaecke";
				
		};

		class Redd_Waffentraeger
		{
					
			displayName = "$STR_Redd_Waffentraeger";
				
		};

		class Redd_Tpz
		{
					
			displayName = "$STR_Redd_Transportpanzer";
				
		};

		class Redd_AA
		{
					
			displayName = "$STR_Redd_Flugabwehr";
				
		};

		class Redd_Wrecks
		{
					
			displayName = "$STR_Redd_Wrecks";
				
		};

		class Redd_Trucks
		{
					
			displayName = "$STR_Redd_Trucks";
				
		};


	};