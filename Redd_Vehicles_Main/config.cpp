	
	
	#include "CfgPatches.hpp"
	#include "CfgEditorClass.hpp"
	#include "CfgCloudlets.hpp"
	#include "CfgMagazines.hpp"
	#include "CfgWeapons.hpp"
	#include "CfgSounds.hpp"
	#include "CfgSoundset.hpp"
	#include "CfgSoundshaders.hpp"
	#include "CfgAnimationSourceSounds.hpp"
	#include "CfgFunctions.hpp"
	#include "CfgInsignia.hpp"
	#include "CfgRscInGameUI.hpp"
	
	class CfgVehicles 
	{

		class thingX;
		class Wreck_Base_F;
		class Box_NATO_AmmoOrd_F;

		class Redd_Milan_Rohr: thingX
		{
		
			displayName = "$STR_Redd_Milan_Rohr_Leer";
			model = "\Redd_Vehicles_Main\data\Redd_Milan_Rohr.p3d";
			scope = 2;
			scopeCurator = 2;
			editorPreview="\redd_vehicles_main\pictures\milan_rohr_pre_picture.paa";
			
		};

		class Redd_Wrecks_Main: Wreck_Base_F
		{

			icon = "iconObject_1x1";
			editorSubcategory = "Redd_Wrecks";
			scope = 1;
			scopeCurator = 1;

		};

		class Redd_Wiesel_TOW_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "$STR_Redd_Wiesel_TOW_Wreck";
			model = "\Redd_Tank_Wiesel_1A2_TOW\Redd_Tank_W_1A2_TOW_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			editorPreview="\redd_vehicles_main\pictures\wiesel_tow_wreck_pre_picture.paa";
			
		};

		class Redd_Wiesel_MK20_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "$STR_Redd_Wiesel_MK20_Wreck";
			model = "\Redd_Tank_Wiesel_1A4_MK20\Redd_Tank_W_1A4_MK20_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			editorPreview="\redd_vehicles_main\pictures\wiesel_mk_wreck_pre_picture.paa";
			
		};

		class Redd_Fuchs_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "$STR_Redd_Fuchs_Wreck";
			model = "\Redd_Tank_Fuchs_1A4\Redd_Tank_Fuchs_1A4_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			editorPreview="\redd_vehicles_main\pictures\fuchs_Wreck_Pre_Picture.paa";
			
		};

		class Redd_Marder_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "$STR_Redd_Marder_Wreck";
			model = "\Redd_Marder_1A5\Redd_Marder_1A5_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			editorPreview="\redd_vehicles_main\pictures\Marder_Wreck_Pre_Picture.paa";
			
		};

		class Redd_Gepard_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "$STR_Redd_Gepard_Wreck";
			model = "\Redd_Tank_Gepard_1A2\Redd_Tank_Gepard_1A2_Wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			editorPreview="\redd_vehicles_main\pictures\Gepard_Wreck_Pre_Picture.paa";
			
		};

		class Redd_Wolf_Wreck: Redd_Wrecks_Main 
		{
		
			displayName = "$STR_Redd_Wolf_Wreck";
			model = "\redd_tank_lkw_leicht_gl_wolf\rnt_wolf_wreck.p3d";
			scope = 2;
			scopeCurator = 2;
			editorPreview="\redd_vehicles_main\pictures\Wolf_Wreck_Pre_Picture.paa";
			
		};
		
		//mortar ammoboxes
		class Redd_Box_120mm_Mo_HE: Box_NATO_AmmoOrd_F 
		{

			displayName = "$STR_Redd_120mm_HE_Box";
			model = "\Redd_Vehicles_Main\data\120mm_Moerser_Kiste_he.p3d";
			editorPreview = "\Redd_Vehicles_Main\data\120mm_Moerser_Kiste_pre.paa";
			icon = "iconCrateOrd";
			maximumLoad = 400;

			class TransportMagazines 
			{
					
				class _xx_Redd_1Rnd_120mm_Mo_shells
				{

					magazine = "Redd_1Rnd_120mm_Mo_shells";
					count =8;

				};

			};

			class TransportItems {};
			class TransportWeapons {};

		};

		class Redd_Box_120mm_Mo_HE_annz: Redd_Box_120mm_Mo_HE 
		{

			displayName = "$STR_Redd_120mm_HE_annz_Box";
			model = "\Redd_Vehicles_Main\data\120mm_Moerser_Kiste_annz.p3d";

			class TransportMagazines 
			{
					
				class _xx_Redd_1Rnd_120mm_Mo_annz_shells
				{

					magazine = "Redd_1Rnd_120mm_Mo_annz_shells";
					count =8;

				};

			};

			class TransportItems {};
			class TransportWeapons {};

		};

		class Redd_Box_120mm_Mo_Smoke: Redd_Box_120mm_Mo_HE 
		{

			displayName = "$STR_Redd_120mm_Smoke_Box";
			model = "\Redd_Vehicles_Main\data\120mm_Moerser_Kiste_nebel.p3d";

			class TransportMagazines 
			{

				class _xx_Redd_1Rnd_120mm_Mo_Smoke_white
				{

					magazine = "Redd_1Rnd_120mm_Mo_Smoke_white";
					count = 8;

				};

			};

		};

		class Redd_Box_120mm_Mo_Illum: Redd_Box_120mm_Mo_HE 
		{

			displayName = "$STR_Redd_120mm_Flare_Box";
			model = "\Redd_Vehicles_Main\data\120mm_Moerser_Kiste_illum.p3d";

			class TransportMagazines 
			{

				class _xx_Redd_1Rnd_120mm_Mo_Flare_white
				{

					magazine = "Redd_1Rnd_120mm_Mo_Flare_white";
					count =8;

				};

			};

		};

		class Redd_Box_120mm_Mo_Combo: Redd_Box_120mm_Mo_HE 
		{

			displayName = "$STR_Redd_120mm_Mixed_Box";
			model = "\Redd_Vehicles_Main\data\120mm_Moerser_Kiste_gemischt.p3d";

			class TransportMagazines 
			{

				class _xx_Redd_1Rnd_120mm_Mo_shells
				{

					magazine = "Redd_1Rnd_120mm_Mo_shells";
					count =3;

				};

				class _xx_Redd_1Rnd_120mm_Mo_annz_shells
				{

					magazine = "Redd_1Rnd_120mm_Mo_annz_shells";
					count =3;

				};

				class _xx_Redd_1Rnd_120mm_Mo_Smoke_white
				{

					magazine = "Redd_1Rnd_120mm_Mo_Smoke_white";
					count = 1;

				};

				class _xx_Redd_1Rnd_120mm_Mo_Flare_white
				{

					magazine = "Redd_1Rnd_120mm_Mo_Flare_white";
					count = 1;

				};

			};

		};

		/*
		class Netz: Redd_Wrecks_Main 
		{
		
			displayName = "Netz";
			model = "\Redd_Vehicles_Main\Tarnnetz_Wolf.p3d";
			scope = 2;
			scopeCurator = 2;
			
		};
		*/

	};