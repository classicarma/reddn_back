

	//Function for Eden attributes to set platton letter and vehicle number

	if (!isServer) exitWith {};
	
	_veh = _this select 0;

	_b = _veh getVariable ["Redd_Marder_1A5_Zug_Buchstabe",""];
	_n = _veh getVariable ["Redd_Marder_1A5_Fzg_Nummer",""];
	
	//sets platoon
	switch toLower (_b) do 
	{
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [2, "\Redd_Vehicles_Main\data\Redd_ZugA_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [2, "\Redd_Vehicles_Main\data\Redd_ZugB_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [2, "\Redd_Vehicles_Main\data\Redd_ZugC_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [2, "\Redd_Vehicles_Main\data\Redd_ZugD_ca.paa"];
		
		};
		
		default {};
		
	};
	
	//sets vehicle number
	switch toLower (_n) do 
	{
	
		
		case ("0"):{};
		
		case ("1"):
		{
		
			_veh setObjectTextureGlobal [3, "\Redd_Vehicles_Main\data\Redd_Zug1_ca.paa"];
				
		};
		
		case ("2"):
		{
		
			_veh setObjectTextureGlobal [3, "\Redd_Vehicles_Main\data\Redd_Zug2_ca.paa"];
		
		};
		
		case ("3"):
		{
		
			_veh setObjectTextureGlobal [3, "\Redd_Vehicles_Main\data\Redd_Zug3_ca.paa"];
				
		};
		
		case ("4"):
		{
		
			_veh setObjectTextureGlobal [3, "\Redd_Vehicles_Main\data\Redd_Zug4_ca.paa"];
		
		};
		
		default {};
		
	};