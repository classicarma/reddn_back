

	//Triggerd by BI eventhandler "getout"
	
	_veh = _this select 0;

	waitUntil {!isNull _veh};
	
	_pos = _this select 1;
	_unit = _this select 2;
	_turret = _this select 3;

	//checks if unit is in Milan
	if (_turret isEqualTo [1]) then
	{

		[_veh,[[0,0],false]] remoteExecCall ['lockTurret']; //unlocks the commander seat which was locked before to prevent units getting in commander seat while actual commander is in milan
		_veh setVariable ['Redd_Marder_Commander_Up', false,true];//resets the variable

	};