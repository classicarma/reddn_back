

    class CfgPatches
	{
		
		class Redd_Tank_LKW_leicht_gl
		{
			
			units[] = 
			{
				
				"Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_FueFu",
				"Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_FueFu",
				"Redd_Tank_LKW_leicht_gl_Wolf_Wintertarn_FueFu",

				"Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_Moerser",
				"Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_Moerser",
				"Redd_Tank_LKW_leicht_gl_Wolf_Winterntarn_Moerser",

				"Redd_Tank_LKW_leicht_gl_Wolf_Flecktarn_San",
				"Redd_Tank_LKW_leicht_gl_Wolf_Tropentarn_San",
				"Redd_Tank_LKW_leicht_gl_Wolf_Winterntarn_San"

			};
			weapons[] = {};
			requiredVersion = 0.1;
			requiredAddons[] = {"A3_Soft_F"};

		};
		
	};