

    class CfgMovesBasic
    {

        class DefaultDie;
        class ManActions
        {

            Redd_Tank_Wiesel_1A2_TOW_Driver = "Redd_Tank_Wiesel_1A2_TOW_Driver";
            Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
            Redd_Tank_Wiesel_1A4_MK_Commander = "Redd_Tank_Wiesel_1A4_MK_Commander";
            Redd_Tank_Wiesel_1A4_MK_Commander_Out = "Redd_Tank_Wiesel_1A4_MK_Commander_Out";
            Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low = "Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low";

            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver";
            KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out = "KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out";
            KIA_Redd_Tank_Wiesel_1A4_MK_Commander = "KIA_Redd_Tank_Wiesel_1A4_MK_Commander";
            KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out = "KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out";
            KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low = "KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low";

        };

    };

    class CfgMovesMaleSdr: CfgMovesBasic
    {

        class States
        {

            class Crew;

            class Redd_Tank_Wiesel_1A2_TOW_Driver: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A2_TOW_Driver_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A2_TOW_Driver_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A4_MK_Commander: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A4_MK_Commander.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A4_MK_Commander",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A4_MK_Commander", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A4_MK_Commander: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A4_MK_Commander.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A4_MK_Commander_Out: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A4_MK_Commander_Out.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

            class Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low: Crew
            {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low.rtm";
                interpolateTo[] = {"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low",1};
                ConnectTo[]={"KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low", 1};
                leftHandIKCurve[] = {1}; 
                rightHandIKCurve[] = {1}; 

            };

            class KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low: DefaultDie
		    {

                file = "\Redd_Tank_Wiesel_1A4_MK20\anims\KIA_Redd_Tank_Wiesel_1A4_MK_Commander_Out_Low.rtm";
                actions = "DeadActions";
                terminal = 1;
                speed = 0.5;
                ragdoll = 1;
                connectTo[] = {"Unconscious",0.1};
                InterpolateTo[] = {};

		    };

        };

    };