

	//triggered by BI eventhandler "turnOut"
	
	params ["_veh","_unit","_turret"];

	waitUntil {!isNull _veh};

	//checks if gunner turns out
	if (_turret isEqualTo [0]) then
	{

		_veh setVariable ['Redd_Gepard_Gunner_Out', true,true];
		_veh setVariable ['Redd_Gepard_Gunner_Up', false,true]; //resets variable for "climp up" function 
		
		if !(_veh getVariable ["Redd_Gepard_Commander_Out",false]) then
		{

			_veh animateSource ["Turmluke_Rot_Source",1];

		};

	};

	//checks if commander turns out
	if (_turret isEqualTo [0,0]) then
	{

		_veh setVariable ['Redd_Gepard_Commander_Out', true,true];
		_veh setVariable ['Redd_Gepard_Commander_Up', false,true]; //resets variable for "climp up" function 
		
		if !(_veh getVariable ["Redd_Gepard_Gunner_Out",false]) then
		{

			_veh animateSource ["Turmluke_Rot_Source",1];

		};

	};