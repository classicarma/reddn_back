

	//triggered by BI eventhandler "getOut"
	
	params ["_veh","_pos","_unit","_turret"];

	waitUntil {!isNull _veh};

	//checks if gunner gets out
	if (_turret isEqualTo [0]) then
	{

		_veh setVariable ['Redd_Gepard_Gunner_Out', false,true];

	};

	//checks if commander gets out
	if (_turret isEqualTo [0,0]) then
	{

		_veh setVariable ['Redd_Gepard_Commander_Out', false,true];

	};