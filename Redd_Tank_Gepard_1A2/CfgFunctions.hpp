
    
    class CfgFunctions
    {

        class Redd
        {

            tag = "Redd";

            class Functions
            {

                class Gepard_Init
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_init.sqf";

                };

                class Gepard_GetOut
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_getOut.sqf";

                };

                class Gepard_GetIn
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_getIn.sqf";

                };

                class Gepard_TurnIn
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_turnIn.sqf";

                };

                class Gepard_TurnOut
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_turnOut.sqf";

                };

                class Gepard_Radar
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_Radar.sqf";

                };

                class Gepard_Fired
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_Fired.sqf";

                };

                class Gepard_Plate
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_plate.sqf";

                };

                class Gepard_Bat_Komp
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_bat_komp.sqf";

                };

                class Gepard_flags
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_flags.sqf";

                };

                class Gepard_cartridge_cover
                {

                    file = "\Redd_Tank_Gepard_1A2\functions\Redd_Tank_Gepard_1A2_cartridge_cover.sqf";

                };

            };

        };

    };